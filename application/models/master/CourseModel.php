<?php

include('httpful.phar');

class CourseModel extends CI_Model {



	var $uri_web='http://190.117.118.40:4444/WSColegio/rest';

	public function getCourse($idsection=null)
	{

		$codmaster= json_decode($_COOKIE["user_data_cookie"],true)['codProfesor'];
		//'http://misitio.local:4444/WSColegio/rest/profesor/listarCursosDeSeccion?idSeccion=27&codProfesor=P00021'
		$ur=web_service_uri.'/profesor/listarCursosDeSeccion?idSeccion='.$idsection.'&codProfesor='.$codmaster;
        $response = \Httpful\Request::get($ur)->send();
        $response=json_decode($response,true);
        return $response;


	}
   public function gettypeEvaluation($idcurso=null)
	{

		$codmaster= json_decode($_COOKIE["user_data_cookie"],true)['codProfesor'];
		//http://misitio.local:4444/WSColegio/rest/evaluacion/listarTipoEvaluacionPorCurso?codCurso=C015
		$ur=web_service_uri.'/evaluacion/listarTipoEvaluacionPorCurso?codCurso='.$idcurso;
        $response = \Httpful\Request::get($ur)->send();
        $response=json_decode($response,true);
        return $response;
	}

	   public function getScore($idtipo_evaluacion=null,$idCourse=null,$idsection=null)
	{
		$codmaster= json_decode($_COOKIE["user_data_cookie"],true)['codProfesor'];
		//evaluacion/listarNotasPorDetalleCriterio?idSeccion=9&codCurso=C015&codProfesor=P00004&codTipoEvaluacion=TE001
		$ur=web_service_uri.'/evaluacion/listarNotasPorDetalleCriterio?idSeccion='.$idsection.'&codCurso='.$idCourse.'&codProfesor='.$codmaster.'&codTipoEvaluacion='.$idtipo_evaluacion;
		/*  $response = \Httpful\Request::get($ur)->send();
			$response=json_decode($response,true);
			return $response;*/
		$ch=curl_init();
		$headers=array('Content-Type', 'application/json;charset=UTF-8');
		$ch=curl_init();
		curl_setopt($ch,CURLOPT_URL,$ur);
		curl_setopt($ch,CURLOPT_HTTPGET,true);
		curl_setopt($ch,CURLOPT_ENCODING, 'UTF-8');
		curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch,CURLOPT_POSTFIELDS,$Evaluations);
		$result=curl_exec($ch);
		if ($result == false) {
			die('Curl failed' . curl_error($ch));
		}
		$result=json_decode($result,true);
		return $result;
	}
	public function setScore($Evaluations=null){
	            $ur=web_service_uri.'/evaluacion/registrarNotas';
            try {
									$ch=curl_init();
									//$headers=array('Content-Type', 'application/json;charset=UTF-8');
									$headers=array(
									    'Content-Type: application/json;charset=UTF-8',
									    'Content-Length: ' . strlen($Evaluations));
									$ch=curl_init();
									curl_setopt($ch,CURLOPT_URL,$ur);
									curl_setopt($ch,CURLOPT_POST,true);
									//curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
									curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
									curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
									curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
									curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
									curl_setopt($ch,CURLOPT_POSTFIELDS,$Evaluations);
									$result=curl_exec($ch);
									if ($result == false) {
										die('Curl failed' . curl_error($ch));
									}

       		/* $response = \Httpful\Request::post($ur)->
					 addHeader(array(
							 'Content-Type: application/json;charset=UTF-8',
							 'Content-Length: ' . strlen($Evaluations)))->
					 body($Evaluations)
					 ->send();*/

					// $response=json_decode($response,true);
           //echo 'correcto';
            } catch (Exception $e) {
               // echo 'fallo';
            }

	}



}
